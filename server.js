const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const fs = require("fs");
const app = express();
const PORT = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(morgan("tiny"));

// post data to the server
app.post("/api/files", async (req, res) => {
  try {
    const {
      filename,
      content
    } = req.body;
    const files = fs.readdirSync("./files");
    if (files.includes(filename)) {
      res
        .status(400)
        .send({
          message: `This '${filename}' file is already saved!`
        });
    }
    if (!filename) {
      res.status(400).send({
        message: "Please specify 'filename' parameter"
      });
    } else if (!content) {
      res.status(400).send({
        message: "Please specify 'content' parameter"
      });
    } else {
      fs.writeFile(`./files/${filename}`, content, (err) => {
        if (err) {
          res.status(500).send({
            message: "Server error"
          });
        }
      });
      res.setHeader("Content-Type", "application/json");
      res.status(200).json({
        message: "File updated successfully",
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Server error"
    });
    return err;
  }
});

// Get data from the server
app.get("/api/files", async (req, res) => {
  const files = fs.readdirSync("./files");
  try {
    if (!files) {
      res.status(400).send({
        message: "Client error"
      });
    }
    res.status(200).send({
      message: "Success",
      files
    });
  } catch (error) {
    res.status(500).send({
      message: "Server error"
    });
  }
});

// Get spesific file from the server
app.get("/api/files/:filename", (req, res) => {
  const {
    filename
  } = req.params;
  const files = fs.readdirSync("./files");
  try {
    if (!files.includes(filename)) {
      res
        .status(400)
        .send({
          message: `No file with '${filename}' filename found`
        });
    }
    const content = fs.readFileSync(`./files/${filename}`);
    const {
      birthtime
    } = fs.statSync(`./files/${filename}`);

    res.status(200).send({
      message: "Success",
      filename,
      content: content.toString(),
      extension: filename.split(".").pop(),
      uploadedDate: birthtime,
    });
  } catch (err) {
    res.status(500).send({
      message: "Server error"
    });
  }
});

app.get("/", (req, res) => {
  res.send("Home page.");
});
app.use((err, req, res, next) => {
  const {
    statusCode = 500
  } = err;
  if (!err.message) err.message = "Server error";
  res.status(statusCode).json({
    message: err.message,
  });
});
app.listen(PORT, () => {
  console.log(`Server running on port http://localhost:${PORT}`);
});